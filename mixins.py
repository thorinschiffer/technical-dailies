class BaseClass:

    def __init__(self):
        self.a = 1

    def abstract_method(self):
        raise NotImplementedError()

    def mixin_method(self):
        print("base")


class Mixin:

    def mixin_method(self):
        print("Mixin")


class Class(BaseClass, Mixin):

    def abstract_method(self):
        return 1


b = BaseClass()
c = Class()

print(c.abstract_method())
c.mixin_method()
b.abstract_method()
