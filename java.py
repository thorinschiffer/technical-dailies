class JavaClass:

    def __init__(self):
        self.__private = None
        self._protected = None

    def setPrivate(self, value):
        self.__private = value

    def getPrivate(self):
        return self.__private


class JavaChildClass(JavaClass):
    def method(self):
        return self._protected


j = JavaClass()
j.setPrivate(1)
print(j.getPrivate())

# print(j.__private)
print(j._JavaClass__private)
print(j.__dict__)
