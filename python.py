class PythonClass:
    def __init__(self):
        self.a = None
        self.b = 2

    @property
    def prop(self):
        return self.b * 2

    @prop.setter
    def prop(self, value):
        self.b = value / 2


p = PythonClass()
p.a = 1
print(p.a)
print(p.__dict__)

print(p.prop)
p.prop = 10
print(p.b)
