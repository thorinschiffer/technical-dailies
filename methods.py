class ClassWithMethods:
    TEST = 1

    @staticmethod
    def static():
        return 1

    @classmethod
    def cls_method(cls):
        cls.TEST = 2


def unbound_function():
    return 1


w = ClassWithMethods()
print(w.static())
w.cls_method()
print(ClassWithMethods.TEST)
print(unbound_function())
